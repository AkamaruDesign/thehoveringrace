﻿using UnityEngine;
using Imputer;

namespace Flyer {
    [RequireComponent(typeof(Rigidbody))]
    public class Fly3 : MonoBehaviour
    { Rigidbody rb;
        float avance, giro, zonaMuerta, vertAxis, horizAxis;
        [SerializeField]
        float potenciaDelMotor, frenos, factorDeAcceleracion, acceleracionFinal;
        [SerializeField]
        float acelearcionAvanace, aceleracionReversa, hoverHeigth, hoverForce, turnSpeed, trackDistance, trackingForce, groundedHeigth;
        [SerializeField]
        Transform[] hoverPoints;
        [SerializeField]
        Transform puntoDeRotacion;
        [SerializeField]
        LayerMask layer;
        public float velocidad;

        Inmputador impt;
        bool grounded;
        AudioSource audio;
        void Start()
        {
            impt = GetComponent<Inmputador>();
            zonaMuerta = 0.1f;
            rb = GetComponent<Rigidbody>();
            audio = GetComponent<AudioSource>();
        }
        private void Update()
        {
            velocidad = (rb.velocity.magnitude) * 3.5f;
            ControlDeAcceleracion();
            giro = 0f;
            horizAxis = impt.xAxis;
            if (Mathf.Abs(horizAxis) > zonaMuerta)
                giro = horizAxis;


        }
        void FixedUpdate()
        {
            if (grounded)
                avance = acelearcionAvanace * acceleracionFinal;
            else
                avance = acelearcionAvanace * (acceleracionFinal / 2);

            ControlDeLevitacion();
            if (Mathf.Abs(avance) > 0)
                rb.AddForce(transform.forward * avance);

            if (giro != 0)
            {
                transform.RotateAround(puntoDeRotacion.position, transform.up, giro * turnSpeed * Time.deltaTime);

                //rb.AddRelativeTorque(Vector3.up * giro * turnSpeed);
            }


            audio.pitch = 1 + acceleracionFinal * 3;
        }
        /*private void OnDrawGizmos()
        {
            RaycastHit hitToFloat, hitToTrack;
            if (Physics.Raycast(transform.position, -transform.up, out hitToTrack, trackDistance))
            {
                Gizmos.color = Color.cyan;
                Gizmos.DrawLine(transform.position, hitToTrack.point);
                Gizmos.DrawSphere(hitToTrack.point, 0.3f);
            }
            else
            {
                Gizmos.color = Color.blue;
                Gizmos.DrawLine(transform.position, transform.position - transform.up * trackDistance);
            }
                foreach (Transform point in hoverPoints)
            {
                if (Physics.Raycast(point.position, -point.up, out hitToFloat, hoverHeigth))
                {
                    Gizmos.color = Color.green;
                    Gizmos.DrawLine(point.position, hitToFloat.point);
                    Gizmos.DrawSphere(hitToFloat.point, 0.2f);
                }
                else
                {
                    Gizmos.color = Color.red;
                    Gizmos.DrawLine(point.position, point.position - point.up * hoverHeigth);
                }
            }

        }*/

        void ControlDeAcceleracion() {
            acceleracionFinal = factorDeAcceleracion / 10;
            if (impt.aBtn)
            {
                if (grounded)
                {
                    factorDeAcceleracion += potenciaDelMotor * Time.deltaTime;
                    if (factorDeAcceleracion > 10f)
                        factorDeAcceleracion = 10f;
                }
            }
            else if (impt.bBtn)
            {
                if (factorDeAcceleracion > 0)
                    factorDeAcceleracion -= (frenos * 3) * potenciaDelMotor * Time.deltaTime;
                if (factorDeAcceleracion <= 0)
                {
                    factorDeAcceleracion -= frenos * Time.deltaTime;
                    if (factorDeAcceleracion < -5f)
                        factorDeAcceleracion = -5f;
                }
            }
            else
            {
                if (factorDeAcceleracion > 0f + zonaMuerta)
                    factorDeAcceleracion -= frenos * potenciaDelMotor * Time.deltaTime;
                else if (factorDeAcceleracion < 0f - zonaMuerta)
                    factorDeAcceleracion += frenos * Time.deltaTime;
                else
                    factorDeAcceleracion = 0f;
                if(velocidad < 5)
                    factorDeAcceleracion = 0f;
            }
        }
        void ControlDeLevitacion() {
            RaycastHit hitToFloat, hitToTrack, hitTheTrack;
            if (!Physics.Raycast(transform.position, -transform.up, out hitTheTrack, groundedHeigth,layer))
            {
                grounded = false;
                //control de elevacion en el  eje X
                if (Mathf.Abs(impt.yAxis) > zonaMuerta)
                {
                    rb.AddRelativeTorque(Vector3.left * -impt.yAxis * (turnSpeed * 0.4f));
                }
                foreach (Transform point in hoverPoints)
                {
                    if (point.position.y > transform.position.y + zonaMuerta)
                        rb.AddForceAtPosition(point.up * -hoverForce * 0.2f, point.position);
                    else if (point.position.y < transform.position.y - zonaMuerta)
                        rb.AddForceAtPosition(point.up * hoverForce * 0.2f, point.position);
                }
            }
            else {

                grounded = true;

            }
            if (Physics.Raycast(transform.position, -transform.up, out hitToTrack, trackDistance))
                rb.AddForce(-transform.up * trackingForce);
            else
                rb.AddForce(-Vector3.up * trackingForce);
            foreach (Transform point in hoverPoints)
            {
                if (Physics.Raycast(point.position, -point.up, out hitToFloat, hoverHeigth))
                    rb.AddForceAtPosition(point.up * hoverForce * (1.0f - (hitToFloat.distance / hoverHeigth)), point.position);
            }
        }

    }
}
