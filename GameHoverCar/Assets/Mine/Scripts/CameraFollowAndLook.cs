﻿using Imputer;
using UnityEngine;
[RequireComponent(typeof(Camera))]

public class CameraFollowAndLook : MonoBehaviour {
    [SerializeField]
    Transform tarject, position;
    [SerializeField]
    float speed;
    Camera cam;
    [SerializeField]
    Inmputador imp;
    Rect rct;
    int players = 4;
    // Use this for initialization
    void Start () {
        cam = GetComponent<Camera>();
        transform.SetParent(null);

        if (players > 1)
        {
            switch (imp.playerID)
            {
                case Inmputador.Player.P1:
                    rct = new Rect(0f, 0.5f, 0.5f, 0.5f);
                    cam.rect = rct;
                    break;
                case Inmputador.Player.P2:
                    rct = new Rect(0.5f, 0.5f, 0.5f, 0.5f);
                    cam.rect = rct;
                    break;
                case Inmputador.Player.P3:
                    rct = new Rect(0f, 0f, 0.5f, 0.5f);
                    cam.rect = rct;
                    break;
                case Inmputador.Player.P4:
                    rct = new Rect(0.5f, 0f, 0.5f, 0.5f);
                    cam.rect = rct;
                    break;
                default:
                    break;
            }
        }
      //  cam.enabled = false;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        Vector3 arriba = Vector3.Slerp(transform.up, tarject.up, (speed/2) * Time.deltaTime);


        transform.LookAt(tarject,arriba);
        Vector3 newPos = Vector3.Lerp(transform.position, position.position, speed * Time.deltaTime);
        transform.position = newPos;
	}
}
