﻿using UnityEngine;
namespace Imputer
{
    public class Inmputador : MonoBehaviour
    {
        public enum Player { Pc, P1, P2, P3, P4 }
        public enum ControlType { generic, bluetooth, weel}
        public Player playerID;
        public ControlType controller;
        public bool playable, aBtn, aBtnDown, bBtn, bBtnDown, xBtn, xBtnDown, yBtn, yBtnDown, 
            l1, l1Down, r1, r1Down, l2, l2Down, r2, r2Down, s, sDown, sC, sCDown;
        public float xAxis, yAxis;

        private void Start()
        {

            
        }

        void Update()
        {
            if (playable)
            {
                if (controller == ControlType.generic)
                {
                    //getAxis
                    xAxis = Input.GetAxis(playerID + "x");
                    yAxis = Input.GetAxis(playerID + "y");
                    //GetButton
                    aBtn = Input.GetButton(playerID + "A");
                    bBtn = Input.GetButton(playerID + "B");
                    xBtn = Input.GetButton(playerID + "X");
                    yBtn = Input.GetButton(playerID + "Y");
                    l1 = Input.GetButton(playerID + "L1");
                    l2 = Input.GetButton(playerID + "L2");
                    r1 = Input.GetButton(playerID + "R1");
                    r2 = Input.GetButton(playerID + "R2");
                    s = Input.GetButton(playerID + "S");
                    sC = Input.GetButton(playerID + "Sc");

                    //GetButtonDown
                    aBtnDown = Input.GetButtonDown(playerID + "A");
                    bBtnDown = Input.GetButtonDown(playerID + "B");
                    xBtnDown = Input.GetButtonDown(playerID + "X");
                    yBtnDown = Input.GetButtonDown(playerID + "Y");
                    l1Down = Input.GetButtonDown(playerID + "L1");
                    l2Down = Input.GetButtonDown(playerID + "L2");
                    r1Down = Input.GetButtonDown(playerID + "R1");
                    r2Down = Input.GetButtonDown(playerID + "R2");
                    sDown = Input.GetButtonDown(playerID + "S");
                    sCDown = Input.GetButtonDown(playerID + "Sc");
                }
                if(controller == ControlType.bluetooth)
                {
                    //getAxis
                    xAxis = Input.GetAxis(playerID + "x");
                    yAxis = Input.GetAxis(playerID + "y");
                    //GetButton
                    aBtn = Input.GetButton(playerID + "Y");
                    bBtn = Input.GetButton(playerID + "B");
                    xBtn = Input.GetButton(playerID + "X");
                    yBtn = Input.GetButton(playerID + "L1");
                    //l1 = Input.GetButton(playerID + "L1");
                    //l2 = Input.GetButton(playerID + "L2");
                    //r1 = Input.GetButton(playerID + "R1");
                    //r2 = Input.GetButton(playerID + "R2");
                    s = Input.GetButton(playerID + "R2");
                    sC = Input.GetButton(playerID + "L2");

                    //GetButtonDown
                    aBtnDown = Input.GetButtonDown(playerID + "Y");
                    bBtnDown = Input.GetButtonDown(playerID + "B");
                    xBtnDown = Input.GetButtonDown(playerID + "X");
                    yBtnDown = Input.GetButtonDown(playerID + "L1");
                    //l1Down = Input.GetButtonDown(playerID + "L1");
                    //l2Down = Input.GetButtonDown(playerID + "L2");
                    //r1Down = Input.GetButtonDown(playerID + "R1");
                    //r2Down = Input.GetButtonDown(playerID + "R2");
                    sDown = Input.GetButtonDown(playerID + "R2");
                    sCDown = Input.GetButtonDown(playerID + "L2");

                }
                if (controller == ControlType.weel)
                {
                    //getAxis
                    xAxis = Input.GetAxis(playerID + "x");
                    if (Input.GetButton(playerID + "A"))
                        yAxis = 1;
                    else
                        if (Input.GetButton(playerID + "Y"))
                        yAxis = -1;
                    else
                        yAxis = 0;
                    //GetButton
                    aBtn = (Input.GetAxis(playerID + "y") < 0);
                    bBtn = (Input.GetAxis(playerID + "y") > 0.1);
                    xBtn = Input.GetButton(playerID + "X");
                    //yBtn = Input.GetButton(playerID + "Y");
                    l1 = Input.GetButton(playerID + "L1");
                    l2 = Input.GetButton(playerID + "L2");
                    r1 = Input.GetButton(playerID + "R1");
                    r2 = Input.GetButton(playerID + "R2");
                    s = Input.GetButton(playerID + "S");
                    sC = Input.GetButton(playerID + "Sc");

                    //GetButtonDown
                    //aBtnDown = Input.GetButtonDown(playerID + "A");
                    //bBtnDown = Input.GetButtonDown(playerID + "B");
                    xBtnDown = Input.GetButtonDown(playerID + "X");
                    yBtnDown = Input.GetButtonDown(playerID + "Y");
                    l1Down = Input.GetButtonDown(playerID + "L1");
                    l2Down = Input.GetButtonDown(playerID + "L2");
                    r1Down = Input.GetButtonDown(playerID + "R1");
                    r2Down = Input.GetButtonDown(playerID + "R2");
                    sDown = Input.GetButtonDown(playerID + "S");
                    sCDown = Input.GetButtonDown(playerID + "Sc");
                }
            }
        }
    }
}


