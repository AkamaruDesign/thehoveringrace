﻿using UnityEngine;

public class TurboController : MonoBehaviour {
    [SerializeField]
    Renderer[] slices;
    [SerializeField]
    float off;
    [SerializeField]
    float speed;
    [SerializeField]
    bool play;
	// Use this for initialization
	void Start () {
        if (play)
        {
            foreach (Renderer item in slices)
            {
                off = 1;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (play)
        {
            foreach (Renderer item in slices)
            {
                off += speed * Time.deltaTime;
                item.material.SetTextureOffset("_MainTex", new Vector2(0, off));
            }
        }
    }
}
