﻿using Flyer;
using UnityEngine;
using UnityEngine.UI;

public class VelocityPrinter : MonoBehaviour {
    Text printer;
    [SerializeField]
    Fly3 player;
    float vel;
	// Use this for initialization
	void Start () {
        printer = GetComponent<Text>();
    }
	
	// Update is called once per frame
	void Update () {
        vel = Mathf.Round(player.velocidad);
        printer.text = vel + "";
	}
}
